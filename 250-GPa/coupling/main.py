import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm

fp = fm.FontProperties(fname = "/System/Library/Fonts/HelveticaNeue.ttc", size = 25)
fp1 = fm.FontProperties(fname = "/System/Library/Fonts/HelveticaNeue.ttc", size = 18)

def read_elph(i):

    x = []
    y = []

    with open("./elph/Li2MgH16.dyn" + str(i) + ".elph." + str(i)) as f:
        for line in f:
            line = line.split()

            if line[0] == "lambda(":
                x.append(float(line[2]))
                y.append(float(line[-2]))

    return x, y

def read_dyn(i):

    x = []

    with open("./elph/Li2MgH16.dyn" + str(i), "r") as f:
        for line in f:
            line = line.split()

            if len(line) == 9 and line[0] == "freq":
                x.append(float(line[4]) * 4.14)

    return x


if __name__ == "__main__":
    gam = 0.5


    for i in range(28):
        fig = plt.figure()
        ax = fig.add_subplot(311)
        ax1 = fig.add_subplot(312)
        ax2 = fig.add_subplot(313)

        x, y = read_elph(i + 1)
        fre  = read_dyn(i + 1)

        omega = []
        phdos = []
        lmdos = []
        a2dos = []

        for xi in np.arange(0, 300, 0.2):
            omega.append(xi)

            dos = 0e0
            lam = 0e0
            a2  = 0e0

            for j in range(len(x)):
                dos += 1e0 / np.pi * gam / ((xi - fre[j]) ** 2e0 + gam ** 2e0)
                lam += 0.5 * fre[j] * x[j] / np.pi * gam / ((xi - fre[j]) ** 2e0 + gam ** 2e0)
                a2  += lam/dos

            phdos.append(dos)
            lmdos.append(lam)
            a2dos.append(lam / dos)

        ax1.set_ylim(0, 15)
        ax2.set_ylim(0, 25)

        ax.plot(omega, phdos, label = r"$\sum_{{\nu}} \delta(\omega - \omega_{q\nu})$")
        ax1.plot(omega, lmdos, label = r"$\alpha^2F(q, \omega)$")
        ax2.plot(omega, a2dos, label = r"$\alpha^2(q, \omega)$")

        for item in ax.xaxis.get_majorticklabels():
            item.set_fontproperties(fp1)

        for item in ax.yaxis.get_majorticklabels():
            item.set_fontproperties(fp1)

        for item in ax1.xaxis.get_majorticklabels():
            item.set_fontproperties(fp1)

        for item in ax1.yaxis.get_majorticklabels():
            item.set_fontproperties(fp1)

        for item in ax2.xaxis.get_majorticklabels():
            item.set_fontproperties(fp1)

        for item in ax2.yaxis.get_majorticklabels():
            item.set_fontproperties(fp1)

        ax.set_xticks([])
        ax1.set_xticks([])
        ax.set_yticks([0, 1, 2, 3])
        ax1.set_yticks([0, 4, 8, 12])
        ax2.set_yticks([0, 10, 20])

        ax.set_title("Q-point " + str(i + 1))

        ax.set_xlabel("") #r"$\Omega$ (meV)", fontproperties = fp)
        ax1.set_xlabel("" ) #r"$\Omega$ (meV)", fontproperties = fp)
        ax2.set_xlabel(r"$\Omega$ (meV)", fontproperties = fp)

        ax.legend(loc = "upper left")
        ax1.legend()
        ax2.legend()


        plt.tight_layout()

        plt.savefig("./Li2MgH16_q=" + str(i + 1) + ".png", format = "png", dpi = 300)

        plt.clf()

